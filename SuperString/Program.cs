﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace SuperString
{
    class MainClass
    {
        // Function that prints an error message and terminates the program
        public static void Error(string message)
        {
            Console.WriteLine(message);
            Environment.Exit(0);
        }

        // Function that returns an array of strings from a file or an empty array, if the files doesn't exists
        public static string[] ReadFile(string path)
        {
            return (File.Exists(path) ? File.ReadAllLines(path) : new string[0]);
        }

        // Function that writes the given string to a given file
        public static void WriteFile(string path, string content)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(path);
            file.WriteLine(content);
            file.Close();
        }

        public static void Main(string[] args)
        {
            List<string> lines = new List<string>();
            string path = "";
            if (args.Length == 0)
            {
                // If no argument with a file is given
                Error("No arguments given");
            }
            else
            {
                // Read file
                path = args[0];
                lines.AddRange(ReadFile(path));
            }

            if (lines.Count == 0)
            {
                // If no lines is recorded, file does not exist
                Error("File does not exist!");
            }

            try
            {
                // Try to create a new SWE object from input
                SWE format = new SWE(lines.ToArray());
                string solution = format.PrintSolution();
                Console.WriteLine(solution);
                WriteFile(Regex.Replace(path, ".SWE", "", RegexOptions.IgnoreCase) + ".SOL", solution);
            }
            catch (FormatException)
            {
                // Catch exception if input is not a valid SWE format
                Error("Invalid SWE format");
            }
        }
    }
}
