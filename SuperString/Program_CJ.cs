﻿// The code used to run the algorithm on CodeJudge

using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace SuperString
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            List<string> lines = new List<string>(); 
            string line;
            // Read every line
            while ((line = Console.ReadLine()) != null && line != "")
                {
                    lines.Add(line);
                }

            try
            {
                // Try to create a new SWE object from input
                SWE format = new SWE(lines.ToArray());
                string solution = format.PrintSolution();
                // Print solution
                Console.WriteLine(solution);
            }
            catch (FormatException)
            {
                // Catch exception if input is not a valid SWE format
                Console.WriteLine("Invalid SWE format");
            }
        }
    }
}
