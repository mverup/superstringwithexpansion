﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SuperString
{
    public class SWE
    {
        // Initializing of variables
        string s;
        List<string> t = new List<string>();
        Dictionary<string, List<string>> R = new Dictionary<string, List<string>>();

        public SWE(string[] input)
        {
            Regex rx;
            int k = 0;
            // Matching first line with a digit
            rx = new Regex(@"^\d+$");
            if (rx.IsMatch(input[0]))
            {
                k = Convert.ToInt32(input[0]);
            }
            else
            {
                throw new FormatException();
            }

            // Matching second line with a string of small letters
            rx = new Regex(@"^[a-z]+$");
            if (rx.IsMatch(input[1]))
            {
                s = input[1];
            }
            else
            {
                throw new FormatException();
            }

            // Matching the next k lines with strings of small and big letters
            rx = new Regex(@"^[a-zA-Z]+$");
            for (int i = 2; i < 2 + k; i++)
            {
                if (rx.IsMatch(input[i]))
                {
                    t.Add(input[i]);
                }
                else
                {
                    throw new FormatException();
                }
            }
            // Remove duplicates by converting to a hashset and back to a list again
            HashSet<string> tmp = new HashSet<string>(t);
            t = new List<string>(tmp);

            // Matching the rest of the lines with a dictionary
            rx = new Regex(@"^[A-Z]:([a-z]+,*)+$");
            for (int i = 2 + k; i < input.Length; i++)
            {
                if (rx.IsMatch(input[i]))
                {
                    string index = input[i].Substring(0, 1);
                    List<string> values = new List<string>(input[i].Substring(2).Split(','));
                    R.Add(index, values);
                }
                else
                {
                    throw new FormatException();
                }
            }
        }

        // Remove entries in the dictionary that doesn't appear in t
        private void RemoveUnused()
        {
            bool[] alphabeth = new bool[26];
            // Running through all strings, t
            foreach (string str in t)
            {
                // Running through all characters in the string
                foreach (char letter in str)
                {
                    // Checking if the character is a small letter
                    if ((int)letter < 91)
                    {
                        // Converting small letters from ascii value to 0-26 indecies
                        // and add set it true in a array
                        alphabeth[(int)letter - 65] = true;
                    }
                }
            }

            for (int i = 0; i < alphabeth.Length; i++)
            {
                if (!alphabeth[i])
                {
                    // If the entry doesn't exist in t, then remove it
                    R.Remove(Convert.ToString((char)(i + 65)));
                }
            }
        }

        // Remove values in R that isn't a substring of s
        private void RemoveNonSubs()
        {
            foreach (List<string> list in R.Values)
            {
                // If the value isn't a substring of s, then remove it from the dictionary
                list.RemoveAll(x => !IsSubstring(x));
            }
        }

        private bool FindSolution()
        {
            Dictionary<string, List<string>> new_dict = new Dictionary<string, List<string>>();
            //checking if any in R is empty
            foreach (KeyValuePair<string, List<string>> entry in R)
            {
                if (entry.Value.Count == 0)
                    return false;
            }

            for (int j = 0; j < this.GetK(); j++)
            {
                new_dict.Clear();
                string temp = t[j];

                for (int i = 0; i < temp.Length; i++)
                {
                    if ((int)temp[i] < 91)
                    {
                        if (i != 0)
                        {
                            if (!temp.Substring(0, i).Contains(temp[i] + ""))
                            {
                                new_dict.Add(temp[i] + "", R[temp[i] + ""]);
                            }

                        }
                        else
                        {
                            new_dict.Add(temp[i] + "", R[temp[i] + ""]);
                        }
                    }
                    else
                    {
                        List<string> mini_letter = new List<string>();
                        mini_letter.Add(temp[i] + "");
                        new_dict.Add(temp[i] + "", mini_letter);
                    }
                }
                Dictionary<string,string> test_solution = new Dictionary<string, string>();
                foreach (string key in new_dict.Keys) //going trough all the keys from t[0]
                {
                    List<string> testing_values = new List<string>(new_dict[key]);
                    int max = new_dict.Keys.Count - 1;
                    int[] pos_sol = new int[max]; //representing all the other keys

                    foreach (string test_value in testing_values)
                    {
                        pos_sol = new int[max];
                        int[] pos_sol_maxes = FindMaxes(new_dict, max, key);

                        bool is_solution = false;
                        while (!IsRestMax(pos_sol, pos_sol_maxes, 0))
                        {
                            test_solution = FindPossibleSolution(pos_sol, key, new_dict);
                            test_solution.Add(key, test_value);

                            if (IsSolution(test_solution, temp))
                            {
                                is_solution = true;
                                break;
                            }

                            pos_sol = CountOneUp(pos_sol, pos_sol_maxes);
                        }
                        //max reached, but not tested
                        if (IsRestMax(pos_sol, pos_sol_maxes, 0))
                        {        
                            test_solution = FindPossibleSolution(pos_sol_maxes, key, new_dict);
                            test_solution.Add(key, test_value);
                            if (IsSolution(test_solution, temp))
                            {
                                is_solution = true;
                            }

                        }
                        if (!is_solution)
                        {

                            new_dict[key].Remove(test_value);
                            R[key].Remove(test_value);
                            if (new_dict[key].Count == 0)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public int[] FindMaxes(Dictionary<string, List<string>> new_dict, int max, string key)
        {
            int index = 0;
            int[] pos_sol_maxes = new int[max];
            foreach (string temp_key in new_dict.Keys)
            {
                if (temp_key != key)
                {
                    pos_sol_maxes[index] = new_dict[temp_key].Count - 1;
                    index++;
                }
            }
            return pos_sol_maxes;
        }

        private Dictionary<string,string> FindPossibleSolution(int[] inpArr, string testing, Dictionary<string,List<string>> inpDic)
        {
            Dictionary<string,string> return_dictionary = new Dictionary<string,string>();
            int index = 0;
            foreach (KeyValuePair<string, List<string>> entry in inpDic)
            {
                if (entry.Key != testing)
                {
                    return_dictionary.Add(entry.Key, entry.Value[inpArr[index]]);
                    index++;
                }
            }

            return return_dictionary;
        }

        // Method removing duplicates from t
        private void RemoveDuplicates()
        {
            for (int i = this.GetK() - 1; i >= 0; i--)
            {
                for (int j = this.GetK() - 1; j >= 0; j--)
                {
                    if (t[i].Contains(t[j]) && i != j)
                    {
                        t.RemoveAt(j);
                        i--;
                    }
                }
            }
        }

        public bool IsSolution(Dictionary <string, string> sol, string input)
        {
            string check_string = "";
            for (int i = 0; i < input.Length; i++)
            {
                if (sol.ContainsKey("" + input[i]))
                    check_string = check_string + sol["" + input[i]];
            }
            return IsSubstring(check_string);
        }

        //counting one up is used for finding the next possible solution in the brute force
        private int[] CountOneUp(int[] input, int[] max)
        {
            bool found_a_max = false;
            int max_index = -1;

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == max[i] && !found_a_max)
                {
                    max_index = i;
                    found_a_max = IsRestMax(input, max, i);
                } 
                if (found_a_max)
                {
                    input[i] = 0;
                }
            }
            if (!found_a_max)
            {
                for (int i = input.Length - 1; i >= 0; i--)
                {
                    if (input[i] != max[i])
                    {
                        input[i]++;
                        break;
                    }
                }
            }
            else
            {
                if (max_index != 0)
                    input[max_index - 1]++;
            }
            return input;
        }

        //used in the count one up to check if the rest of an array is reached the max points
        private bool IsRestMax(int[] input, int[] max, int index)
        {
            for (int i = index; i < input.Length; i++)
            {
                if (input[i] != max[i])
                {
                    return false;
                }
            }
            return true;
        }

        // Perform all optimization steps
        private void Optimize()
        {
            RemoveUnused();
            RemoveNonSubs();
            RemoveDuplicates();
        }

        // Find solution
        private Dictionary<string, string> GetSolution()
        {
            // Perform optimization
            this.Optimize();
            bool solution_found = FindSolution();
            if (solution_found)
                solution_found = FindSolution();
            if (solution_found)
            {
                // If a solution is found
                Dictionary<string, string> sol = new Dictionary<string, string>();
                foreach (KeyValuePair<string, List<string>> entry in R)
                {
                    // Add solution to a dictionary
                    sol.Add(entry.Key, entry.Value[0]);
                }
                // Return dictionary with solution
                return sol;
            }
            // If no solution is found, return null
            return null;
        }

        // Give string with solution
        public string PrintSolution()
        {
            string output = "";
            Dictionary<string, string> sol = this.GetSolution();
            if (sol == null)
            {
                // If a solution is not found, return NO
                return "NO";
            }
            foreach (KeyValuePair<string, string> entry in sol)
            {
                // Add the solutions to the string
                output += entry.Key + ":" + entry.Value + "\n";
            }
            // Return the solution in a string
            return output.Trim();
        }

        // Number of strings, t
        public int GetK()
        {
            return t.Count;
        }

        // Test if a given string is a substring of s
        private bool IsSubstring(string str)
        {
            return s.Contains(str);
        }

        // Print out the problem in the valid SWE format
        public override string ToString()
        {
            string output = GetK() + "\n" + s;
            foreach (string entry in t)
            {
                output += "\n" + entry;
            }
            foreach (KeyValuePair<string, List<string>> entry in R)
            {
                output += "\n" + entry.Key + ":" + String.Join(",", entry.Value);
            }
            return output;
        }
    }
}
